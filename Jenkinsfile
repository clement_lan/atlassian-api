#!/usr/bin/groovy
//  Jenkins Declarative Pipeline
//
//  tri
//
//  \Author Hans Kramer
//
//  \Date   May 2018
//
//  Annoyances :
//     - SVN versions differ accross FC20 and CentOS7
//       FIX: docker exec ${CONTAINER_ID} svn upgrade . || true
//
//     - Weird docker bug on RPM database
//       FIX: docker exec ${CONTAINER_ID} touch /var/lib/rpm/*
//
//     - rpmbuild creates root owned file, which jenkins cannot delete
//       FIX: docker exec ${CONTAINER_ID} chown -R geeds:geeds *
//

library 'eds@1.2.0'


// Get project name from Multipipeline Name
// This is assumed to be the basename of the SPEC file
// For projects for which this is not the case, this has to be modified 

def project_name = (JOB_NAME.tokenize(' /') as String[])[0]


//
// It was hard to write, so it should be hard to read
//

pipeline {
    parameters {
        choice(name: 'platform', choices: 'fc20\ncentos7', description: 'Build Platform')
        booleanParam(name: 'build',            defaultValue: true,  description: "Compile and build")
        booleanParam(name: 'build_docu',       defaultValue: false, description: "Create Documentation")
        booleanParam(name: 'cpp_check',        defaultValue: false, description: "Run CPP Checks")
        booleanParam(name: 'unit_test',        defaultValue: false, description: "Run Unit tests")
        booleanParam(name: 'publish',          defaultValue: true,  description: "Publish the RPMs to the repo")
        booleanParam(name: 'debug_rpm',        defaultValue: false, description: "Create debug RPM")
        booleanParam(name: 'debug_build',      defaultValue: false, description: "Debug. If selected then containers and workspace are NOT deleted")
        booleanParam(name: 'cache',            defaultValue: true,  description: "Use the cached docker image")
        string(name: 'email',                  defaultValue: '',    description: 'Email address to send notifications to')
        booleanParam(name: 'email_build_user', defaultValue: false, description: "Email the user that kicked off the build")
        booleanParam(name: 'push_anyway',      defaultValue: false, description: "Always push container to docker registry")

    }

    options {
        buildDiscarder(logRotator(numToKeepStr: '50'))
        disableConcurrentBuilds()
    }

    agent {
        label  {
            label           'buildnodes'
            customWorkspace "/home/collab/workspace/${params.platform}/$project_name/${BRANCH_NAME}"
        }
    }

    stages {
        stage('Prep') {
            steps {
                script {
                    utils.prelude(project_name)

                    reports.init(project_name)
                }
            }
        }

        stage('Init Docker') {
            steps {
                script {
                    utils.print_stage_header("Init Docker")

                    String app_version = utils.get_app_version(project_name, params.platform)

                    container.kill(project_name)

                    if (utils.previous_build_failed()) {
                        echo "last build failed.... cleanup containers"
                        container.cleanup()
                    }

                    container.start(project_name, params.platform, app_version)
                }
            }
        }

        stage('Dependencies') {
            when {
                expression { return params.build || params.build_docu || params.cpp_check }
            }
            steps {
                script {
                    utils.print_stage_header("Dependencies")

                    if (build.prep(project_name, "${BRANCH_NAME}")) { // load deps
                        echo "Container got updated"

                        String app_version = utils.get_app_version(project_name, params.platform)
               
                        container.push(project_name, "${BRANCH_NAME}", app_version)
                    } 
                }
            }
        }

        stage('Build') {        
            parallel {
                stage('1. Compile') {
                    when {
                        expression { return params.build }
                    }
                    steps {
                        script {
                            utils.print_stage_header("Compile")

                            build.build(project_name)
                        }
                    }
                }

                stage('2. CPP Check') {        
                    when {
                        expression { return params.cpp_check }
                    }
                    steps {
                        script {
                            utils.print_stage_header("CPP Check")

                            results = build.cpp_check(project_name, "cpp-checks") as String[]
                            
                            reports.add("cpp-checks")
 
                            utils.print(results)
                        }
                    }
                }

            }
        }

        stage('Build Docu') {
                    when {
                        expression { return params.build_docu }
                    }
                    steps {
                        script {
                            utils.print_stage_header("Build Docu")

                    build.docu(project_name)
                }
            }
        }

        stage('Unit Tests') {
            when {
                expression { return params.unit_test && params.build}
            }
            steps {
                script {
                    utils.print_stage_header("Unit Tests")

                    results = build.unit_tests(project_name, "unit-tests") as String[]

                    reports.add("unit-tests")

                    utils.print(results)
                }
            }
        }

        stage('Publish') {        
            when {
                expression { return params.build && params.build }
            }
            steps {
                script {
                    utils.print_stage_header("Publish")

                    results = build.publish(project_name, "${BRANCH_NAME}", params.platform)    

                    reports.add("rpms-published")

                    utils.print(results)
                }
            }
        }

        stage('Reports') {
            agent { label 'master' }

            steps {
                script{ 
                    utils.print_stage_header("Reports")

                    reports.publish()
                }
            }
        }
    }

    post {
        cleanup {
            script {
                utils.print_stage_header("Post clean up")

                if (params.debug_build == false) {
                    container.kill(project_name)

                    container.cleanup()

                    utils.delete_workspace()
                } else {
                    utils.clean_workspace()
                }
            }
        }

        always {
            script {
                utils.print_stage_header("Post always")

                String[] rpms = build.rpms()

                utils.print(rpms)

                utils.notify_build_results(project_name, rpms)
            }
        }
    }
}

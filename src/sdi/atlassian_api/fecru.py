#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Fecru.py

Class for handling calls to Fecru endpoints
"""

import logging
from request_wrapper import RequestWrapper
from requests import HTTPError


class Fecru(object):
    """
    Fecru class is responsible for all calls for fisheye/crucible
    """

    def __init__(self, user, password, url):
        """
        Initialize the class
        :param user: Username for HTTP auth
        :param password:  Password for HTTP auth
        :param url:  base URL for calls
        """
        self._url = url
        self._user = user
        self._password = password
        self._req_handler = RequestWrapper(self._user, self._password)
        self._log = logging.getLogger("atlassian-api")

    def get_repo_name(self, project_key):
        """
        Search for the repo, filtering by component name.
        We assume component name == repository name
        """
        try:
            data = self._req_handler.get(self._url + "rest-service-fecru/admin/projects/%s" % project_key)
            return data["defaultRepositoryName"]
        except HTTPError:
            self._log.error("Unable to find project using key: %s", project_key)
            return None

    def repo_exists(self, repo_name):
        """
        Checks if a repo by the given name exists.
        :param repo_name: Name of repository to check for
        :except HTTPError
        """
        try:
            # We do get data back from this, but we don't really care. As long as we get a non-404 response it exists.
            self._req_handler.get(self._url + "rest-service-fecru/admin/repositories/%s" % repo_name)
            return True
        except HTTPError:
            self._log.error("Unable to find repository with key %s", repo_name)
            return False

    def get_required_reviewers(self, repo_name):
        """
        Returns list of required reviewers. Returns empty list for now.
        :param repo_name: Name of repository for which reviewers are required
        """
        # FIXME: Needs to be implemented
        return []

    def get_reviews(self, issue_id, complete_only=False):
        try:
            # FIXME: Are we going to continue using default project for everybody?
            params = {
                "projectKey": "CR",
                "creator": self._user,
                "states": "Draft,Approval,Review",
                "orRoles": "true",
                "title": issue_id
                }
            if complete_only:
                params["allReviewersComplete"] = "true"

            data = self._req_handler.get(self._url + "rest-service/reviews-v1/filter/", req_params=params)
            # FIXME: Apparently the issue id does not count when searching for title.
            #  So, we need to filter based on name data from reviewData
            return [x for x in data["reviewData"] if issue_id in x["name"]]
        except HTTPError:
            self._log.error("Unable to retrieve review data")
            return []

    def get_completed_reviews(self, issue_id):
        """
        Retrieves list of completed reviews
        :param issue_id: ID of JIRA issue
        :return: List of reviews which have been completed
        """
        return self.get_reviews(issue_id, complete_only=True)

    def create_review(self, data):
        """
        Create a new review in Crucible
        :param data: Data to use in review creation
        :return response from API, None if error
        """
        try:
            response_data = self._req_handler.post(self._url + "rest-service/reviews-v1", req_data=data)
            return response_data
        except HTTPError as e:
            self._log.error("Unable to create new review: %s", str(e))
            return False

    def complete_review(self, review_id, summary):
        """
        Mark review as completed
        :param review_id: perma-id of review
        :param summary: summary of review
        :return response from API, None if error
        """
        try:
            data = self._req_handler.post("%srest-service/reviews-v1/%s/close" % (self._url, review_id),
                                          req_data=summary)
            return data
        except HTTPError as e:
            self._log.error("Unable to complete review %s: %s", review_id, str(e))
            return False

    def get_uncompleted_reviewers(self, review_id):
        """
        Gets list of people who haven't completed the review
        :param review_id: perma-id of review
        :return response from API, None if error
        """
        try:
            data = self._req_handler.get("%srest-service/reviews-v1/%s/reviewers/uncompleted"
                                         % (self._url, review_id))
            return data
        except HTTPError as e:
            self._log.error("Unable to get completed reviewers for review %s (%s)", review_id,  e.response.text)
            return False
        
    def get_reviewers(self, review_id):
        """
        Get reviewers listed on a review
        :param review_id: ID of review
        :return: list of reviewers or False
        """
        try:
            data = self._req_handler.get("%srest-service/reviews-v1/%s/reviewers"
                                         % (self._url, review_id))
            return [reviewer["userName"] for reviewer in data["reviewer"]]
        except HTTPError as e:
            self._log.error("Unable to get list of reviewers for review %s (%s)", review_id,  e.response.text)
            return False

    def add_reviewers(self, review_id, reviewers):
        """
        Add reviewers to review
        :param review_id: ID of review
        :param reviewers: list of reviewers to add
        :return: response from API or False
        """
        try:
            data = self._req_handler.post_raw("%srest-service/reviews-v1/%s/reviewers"
                                              % (self._url, review_id), req_data=reviewers)
            return data
        except HTTPError as e:
            self._log.error("Unable to add reviewers for review %s (%s)", review_id,  e.response.text)
            return False

    def delete_reviewer(self, review_id, reviewer):
        """
        Remove reviewer from list of reviewers
        :param review_id: ID of review
        :param reviewer: Reviewer to delete
        :return: True if successfully deleted else False
        """
        try:
            self._req_handler.delete("%srest-service/reviews-v1/%s/reviewers/%s"
                                     % (self._url, review_id, reviewer))
            return True
        except HTTPError as e:
            self._log.error("Unable to delete reviewer for review %s (%s)", review_id, e.response.text)
            return False

    def update_branch_review(self, review_id, branch_data):
        """
        Add branches to a review
        :param review_id: ID of review
        :param branch_data: data to update review with
        :return: True if success else False
        """
        try: 
            self._req_handler.post("%srest/branchreview/latest/trackedbranch/%s"
                                   % (self._url, review_id), req_data=branch_data)
            return True
        except HTTPError as e:
            self._log.error("Unable to update branch review %s (%s)", review_id,  e.response.text)
            return False

    def get_branch_review_details(self, review_id):
        """
        Get branch review data
        :param review_id: ID of review
        :return: data from response else False
        """
        try: 
            data = self._req_handler.get("%srest/branchreview/latest/trackedbranch/%s"
                                         % (self._url, review_id))
            return data
        except HTTPError as e:
            self._log.error("Unable to retrieve details review %s (%s)", review_id,  e.response.text)
            return False

    def refresh_branch_review(self, review_id, _id):
        """
        Force an update on a branch-based review
        :param review_id: ID of review
        :param _id: ID of branch to update
        :return: True if success else False
        """
        try: 
            self._req_handler.put("%srest/branchreview/latest/trackedbranch/%s/%s/refresh"
                                  % (self._url, review_id, _id))
            return True
        except HTTPError as e:
            self._log.error("Unable to refresh branch review %s (%s)", review_id,  e.response.text)
            return False

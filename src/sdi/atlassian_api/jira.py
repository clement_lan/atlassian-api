#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Jira.py

Class for handling JIRA endpoints
"""

import logging
from request_wrapper import RequestWrapper
from requests import HTTPError


class Jira(object):
    def __init__(self, url, user, password):
        self._url = url
        self._user = user
        self._password = password
        self._req_handler = RequestWrapper(self._user, self._password)
        self._log = logging.getLogger("atlassian-api")

    def test_login(self):
        """
        Function for testing login information. We don't wrap this in a try/catch because we want the error to propagate
        """
        self.myself()

    def get_issue_data(self, issue_id):
        """
        Gets data associated with issue_id.

        Uses TrackerApp service method to get data.

        :param issue_id: A string holding artifact id of issue whose data is returned.
        :return: JSON containing artifact information or None if nothing was found
        :except: requests.HTTPError
        """
        try:
            data = self._req_handler.get(self._url + "issue/" + issue_id,
                                         req_params={"fields": "fixVersions,summary,project"})
            return data
        except HTTPError:
            self._log.error("%s doesn't exist, or insufficient JIRA permissions", issue_id)
            return None

    def myself(self):
        return self._req_handler.get(self._url + "myself")

    def is_admin(self, project_key):
        """
        Checks if user is admin of project given project data structure.
        Gets a list of project admins by getting project id from project
        data structure and checks if user is in the list.

        :param: project_key: A project key string.
        :return: True if user is an admin of project associated with data structure given.
                 False if user is  not an admin of project or there is a requests.HTTPError exception.
        :except: requests.HTTPError
        """
        try:
            project_data = self._req_handler.get(self._url + "project/%s" % project_key)
            admin_data = self._req_handler.get(project_data["roles"]["Administrators"])
            myself_data = self.myself()
            for actor in admin_data["actors"]:
                if actor["name"] == myself_data["name"]:
                    return True
        except HTTPError as e:
            self._log.error("Unable to get role data: %s", e.response.text)
            return False
        return False


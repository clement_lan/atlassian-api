#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
RequestWrapper.py

Wrapper around requests library, filling out common headers, auth, logging, and exception handling
"""

import logging
import requests
import json


class RequestWrapper(object):
    """
    Thin wrapper object to set some common things e.g. headers, auth for the requests calls
    """

    def __init__(self, user, password):
        """
        Constructor for RequestsWrapper.

        :param user: A string that holds username for authentication.
        :param password: A string that holds password for authentication.
        """

        self._user = user
        self._password = password
        self._req_headers = {'Accept': 'application/json, text/plain, */*',
                             'content-type': 'application/json'}
        self._log = logging.getLogger("atlassian-api")
        self._auth = requests.auth.HTTPBasicAuth(self._user, self._password)

    def _request(self, dest_url, request_type, req_params=None):
        """
        Internal function to handle all types of calls to requests. Sets the header and auth for the caller.

        :param dest_url: URL to call request on
        :param req_params: parameters used in request
        :param request_type: String representing request type, e.g. GET, POST, PUT, DELETE
        :return JSON response where applicable or empty dict
        """
        self._log.debug("Processing request %s on %s with params %s", request_type, dest_url, json.dumps(req_params))

        if request_type == "GET":
            response = requests.get(dest_url, headers=self._req_headers, auth=self._auth, params=req_params)
        elif request_type == "POST":
            response = requests.post(dest_url, headers=self._req_headers, auth=self._auth, data=json.dumps(req_params))
        elif request_type == "POST_RAW":
            # Special case where we don't want to send as application/json
            raw_headers = self._req_headers
            del(raw_headers['content-type'])
            response = requests.post(dest_url, headers=raw_headers, auth=self._auth, data=req_params)
        elif request_type == "PUT":
            response = requests.put(dest_url, headers=self._req_headers, auth=self._auth, data=req_params)
        elif request_type == "DELETE":
            # Is there ever a scenario where you provide request params to a DELETE endpoint?
            response = requests.delete(dest_url, headers=self._req_headers, auth=self._auth)
        else:
            self._log.error("Invalid request type: %s", request_type)
            raise TypeError("Expected REST API call type but got: %s" % request_type)

        self._log.debug("Got response code for %s %s: %s", request_type, response.url, response.status_code)
        response.raise_for_status()
        # There are cases where we do not get JSON data as part of the response.
        # This handles that case while returning sane data.
        try:
            self._log.debug("Got Response data for %s %s: %s", request_type, dest_url, str(response.json()))
            return response.json()
        except ValueError:
            # This means that we got a 200 code but there was no json response data.
            # For now let's just return an empty dict.
            self._log.warn("No json content found, but got: %s", response.text)
            return {}

    def get(self, dest_url, req_params=None):
        """
        Internal helper function to handle the construction of GET request. Always returns JSON.

        :param dest_url: Endpoint to call
        :param req_params: GET request parameters
        """
        return self._request(dest_url, "GET", req_params)

    def post(self, dest_url, req_data=None):
        """
        Internal helper function to handle the construction of POST request

        :param dest_url: Endpoint to call
        :param req_data: POST request data. Defaults to None
        """
        return self._request(dest_url, "POST", req_data)

    def post_raw(self, dest_url, req_data=None):
        """
        Internal helper function to handle the construction of POST request without conversion of data to json string

        :param dest_url: Endpoint to call
        :param req_data: POST request data. Defaults to None
        """
        return self._request(dest_url, "POST_RAW", req_data)

    def put(self, dest_url, req_data=None):
        """
        Internal helper function to handle the construction of PUT request

        :param dest_url: Endpoint to call
        :param req_data: PUT request data
        """
        return self._request(dest_url, "PUT", req_data)

    def delete(self, dest_url):
        """
        Internal helper function to handle the construction of GET request

        :param dest_url: Endpoint to call
        """
        return self._request(dest_url, "DELETE")

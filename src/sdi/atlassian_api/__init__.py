
import logging

# Set up the logger for the modules
logger = logging.getLogger('atlassian-api')
logger.setLevel(logging.INFO)

# create console handler
console_handler = logging.StreamHandler()
console_handler.setLevel(logging.ERROR)
# create formatter and add it to the handlers
formatter = logging.Formatter("%(asctime)s|%(module)s|%(levelname)s|%(funcName)s|%(message)")
console_handler.setFormatter(formatter)
# add the handlers to the logger
logger.addHandler(console_handler)


def set_log_level(loglevel):
    """
    Function to set log level for whole module
    :param loglevel: logleel to set to
    :return: N/A
    """
    logger.info("Setting log level for atlassian-api to %s", loglevel)
    logger.setLevel(loglevel)

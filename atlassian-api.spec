Name: atlassian-api
Version: 1.0.0
Release: %{svn_rev}%{?dist}
License: Smiths Detection, LLC
Summary: Atlassian API wrapper
BuildRequires: make, rpm-build, cmake, python-mock, doxygen, pytest
BuildArch: noarch

%description
Convienience module to expose Atlassian API in python module

%install
mkdir -p %{buildroot}%{python2_sitelib}/sdi/

cp -r %{SVN_ROOT}/src/sdi/* %{buildroot}%{python2_sitelib}/sdi/
cp -r %{SVN_ROOT}/Pipfile %{buildroot}%{python2_sitelib}/sdi/atlassian-api/requirements.txt

%files
# This may cause a conflicts with svntool
%attr(755,-,-) %{python2_sitelib}/sdi/

%post
pip install -r %{python2_sitelib}/sdi/atlassian-api/requirements.txt

%changelog
* Thu Mar 21 2019 Clement Lan <clan@smiths-detection.com>
- Version 1.0.0

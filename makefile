include version

SVN_ROOT := $(shell pwd)
SVN_REVISION := $(shell svn info | sed -n 's/^Revision: \(.*\)$$/\1/p')

all:    build

clean:
	rm -rf build test

#phony causes 'make test' to run even if test folder exists
.PHONY: test doc
# test program accepts one of the following
# make test || make SVNUSER=<name> SVNPASS=<password> test
test:     setup
	cd src && python -m pytest ../test/ --log-cli-level DEBUG

deps:
	yum-builddep -y atlassian-api.spec --enablerepo=fedora,updates

setup:
	mkdir -p build && cd build && cmake ..

makecache:
	yum makecache --enablerepo=fedora,updates

doc:	setup
	cd build && make doc && rm -rf doc && mkdir -p doc && cd ../ && doxygen

rev_file:
	echo 'version = "${MAJOR_VERSION}.${MINOR_VERSION}.${PATCH_LEVEL}"' > src/sdi/atlassian-api/version.py

pre_build:	doc rev_file

build:	pre_build
	rpmbuild -bb -D "MAJOR_VERSION ${MAJOR_VERSION}" -D "MINOR_VERSION ${MINOR_VERSION}" -D "PATCH_LEVEL ${PATCH_LEVEL}" \
                     -D "svn_rev ${SVN_REVISION}" -D "SVN_ROOT ${SVN_ROOT}" \
                     "${SVN_ROOT}/atlassian-api.spec"
